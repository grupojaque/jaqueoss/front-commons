#!/bin/bash
find . -regex './website/docs/.*\.md' -exec gawk -i inplace -f ./scripts/format-docs.awk '{}' ';'
gawk -v "n=3" -v "s=    Introduction: ['index']," '(NR==n) { print s } 1' website/website/sidebars.js > website/sidebars.js
