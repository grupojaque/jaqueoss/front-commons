/.*/ {
  if (NR <= 4) {
    for (i = 1; i <= NF; i++) {
      if (match($i, /([^ \/]+\/)+([^ \/\.]+)\.([^ \/]+)/, matchparts))
      {
        filename = matchparts[2];
        fileextension = matchparts[3];
        $i = sprintf("%s%s", toupper(substr(filename, 1, 1)), substr(filename, 2));
        $i = gensub(/-/, " ", "G", $i);
      }
      if (match ($i, /"([A-Z][a-z0-9]+)+"/, matchparts)) {4
        $i = matchparts[0];
        $i = gensub(/"/, "", "G", $i)
        $i = gensub(/([a-z])([A-Z])/, "\\1 \\2", "G", $i)
        $i = tolower($i);
        $i = sprintf("%s%s", toupper(substr($i, 1, 1)), substr($i, 2));
      }
    }
  }
  print $0;
}
