# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [`v0.1.0-beta.2`]

### Changed

- Modified the name of the `FileValidation` related `enum` and `model`, together with their files.
- Fixed the GitLab CI configuration file, there were missing packages and misconfiguration of the scripts.
- Fixed the Docusaurus `url` and `base-url`, website wasn't rendering properly.

## [`v0.1.0-beta.1`]

### Added

- Create a Regex helper that includes regexps for `email`, `password`, `YouTube URL`,
  `YouTube Video IDs`, and `urls`.
- Use [Typedoc](https://typedoc.org/) to generate docs from the source code.
- Use [Docusaurus](https://docusaurus.io/) to build the docs website.
- Add GitLab CI configuration to publish the generated Docusaurus website.

## [`v0.1.0-alpha`]

### Added

- Submit the first helpers that have served previous Jaque projects, which include:
  - File helpers (validation with common errors, and image conversion to base64)
  - Dom helpers (password display switch)
  - Form helpers (mark Angular form controls as touched)
  - General helpers (data helpers to validate path existence in objects, convert enums into arrays,
    and find objects that match a comparison inside an object array)

[unreleased]: https://gitlab.com/grupojaque/jaqueoss/front-commons/-/compare/v0.1.0-beta.1...development
[`v0.1.0-beta.2`]: https://gitlab.com/grupojaque/jaqueoss/front-commons/-/compare/v0.1.0-beta.1...v0.1.0-beta.2
[`v0.1.0-beta.1`]: https://gitlab.com/grupojaque/jaqueoss/front-commons/-/compare/v0.1.0-alpha...v0.1.0-beta.1
[`v0.1.0-alpha`]: https://gitlab.com/grupojaque/jaqueoss/front-commons/-/releases#v0.1.0-alpha
