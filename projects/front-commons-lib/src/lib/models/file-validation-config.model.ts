export class FileValidationConfig {
  /**
   * Indicates max size in bytes
   */
  fileMaxSize: number;
  /**
   * Indicates the types accepted, example: ['image/png', 'image/jpg']
   */
  fileTypesAccepted: string[];
}
