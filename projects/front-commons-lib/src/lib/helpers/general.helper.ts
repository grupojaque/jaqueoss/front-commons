/**
 * General Helper
 *
 * This module implements functions that we've used (and even implemented) repeatedly
 * in the same project. Most of the functions in here are assumed to be requiered at
 * least once in common front-end projects.
 *
 *
 * @packageDocumentation
 */

/**
 * Check if a deep property path is defined in an object
 *
 * The path is received as a string separated with dots, as you'd otherwise
 * try to access those properties: `prop.prop2.prop3`.
 *
 * If **any** property in the path is undefined, the function will return false.
 *
 * ```ts
 * // returns true, `deep.deeper.a` is defined
 * objectHasPath(
 *    {shallow: '', deep: {one: 1, two: 2, deeper: { a: 'a'}}},
 *    "deep.deeper.a")
 *
 * // returns false, `deep.deeper.deepest` is undefined
 * objectHasPath(
 *    {shallow: '', deep: {one: 1, two: 2, deeper: { a: 'a'}}},
 *    "deep.deeper.deepest.a")
 * ```
 *
 * @category Data Handling
 *
 * @param obj Object to find the propety path
 * @param path Dot-separated string representing the property path to check
 * @returns Whether all the properties in the path are defined or not
 */
export function objectHasPath(obj: any, path: string): boolean {
  // Obtain a list of anidated properties to check
  const props = path.split('.');

  // Create a current object that will hold whichever level it has last accessed
  let currentObj = { ...obj };

  for (const prop of props) {
    // When any of the properties is undefined, return false
    if (currentObj[prop] === undefined) {
      return false;
    }

    // Update the current object to check next level
    currentObj = { ...currentObj[prop] };
  }

  return true;
}

/**
 * Check if an object contains all of many deep property paths
 *
 * Each path is received as a string separated with commas, as you'd otherwise
 * try to access those properties: `prop.prop2.prop3`.
 *
 * If **any** of the paths in the paths array is undefined, the function will
 * return false.
 *
 * ```ts
 * // returns true | All paths are defined
 * hasPropertyPaths(
 *    {shallow: '', deep: {one: 1, two: 2, deeper: { a: 'a'}}},
 *    ["shallow", "deep.one", "deep.deeper.a"])
 *
 * // returns false | `deep.deeper.deepest` is undefined
 * hasPropertyPaths(
 *    {shallow: '', deep: {one: 1, two: 2, deeper: { a: 'a'}}},
 *    ["shallow", "deep.one", "deep.deeper.deepest.a"])
 * ```
 *
 * @category Data Handling
 *
 * @param obj Object to find the propety paths
 * @param paths Each string represents the path to check
 * @returns Whether each path in the list is defined or not
 */
export function objectHasPaths(obj: any, paths: string[]): boolean {
  // Keep only the paths that exist in the object. If the number of valid paths
  // equals the number of paths to check, all of them are valid
  return (
    paths.filter((path) => objectHasPath(obj, path)).length === paths.length
  );
}

/**
 * Return the value that an object holds in a given path
 *
 * When the path doesn't exist in any of the anidated keys, it'll return `undefined`.
 *
 * ```ts
 * // returns `'a'`
 * getObjectPath(
 *    {shallow: '', deep: {one: 1, two: 2, deeper: { a: 'a'}}},
 *    "deep.deeper.a");
 *
 * // returns `undefined`
 * getObjectPath(
 *    {shallow: '', deep: {one: 1, two: 2, deeper: { a: 'a'}}},
 *    "myprop");
 * ```
 *
 * @category Data Handling
 *
 * @param obj An object
 * @param path Path to access, example "pagination.limit"
 */
export function getObjectPath(obj: any, path: string) {
  const paths = path.split('.');
  let current = obj;
  while (paths.length) {
    if (typeof current !== 'object') {
      return undefined;
    }
    current = current[paths.shift()];
  }
  return current;
}

/**
 * @param obj is an enum
 * @returns object array
 */
export function getArrayOfAnEnum<T>(
  obj: T
): Array<{ label: string; value: string | number | boolean }> {
  const array: Array<{
    label: string;
    value: string | number | boolean;
  }> = [];

  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      array.push({
        label: obj[key].toString(),
        value: key,
      });
    }
  }
  return array;
}

/**
 * Returns the first element that matches a shallow comparison
 *
 * For every object in the array, it'll try to fetch the property value
 * for the received path and shallow-compare it to the value to match.
 *
 * ```ts
 * // returns `{a: 30, b: 40}`
 * findValueInObjectArray([{a: 10}, {b: 20}, {a: 30, b: 40}], "a", 30)
 *
 * // returns `undefined`
 * findValueInObjectArray({}, "a", 30)
 * ```
 *
 * @param array Array of objects to look in
 * @param path Property path of the value to compare
 * @param value Value that matches the searched object
 */
export function findValueInObjectArray(
  array: any[],
  path: string,
  value: string | boolean | number
): any {
  for (const obj of array) {
    const found = getObjectPath(obj, path);
    if (found === value) {
      return obj;
    }
  }
  return null;
}
