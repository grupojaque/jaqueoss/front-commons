/**
 * # DOM Helper
 *
 * This module contains methods that directly interact with elements of the DOM
 *
 * @packageDocumentation
 */

import { ElementRef } from '@angular/core';

/**
 * Override attribute type of element with the values `text` or `password`
 *
 * @param input Input of type [ElementRef](https://angular.io/api/core/ElementRef)
 */
export function switchShowHidePassword(input: ElementRef) {
  const type = input.nativeElement.getAttribute('type');
  if (type) {
    if (type === 'password') {
      input.nativeElement.setAttribute('type', 'text');
    } else {
      input.nativeElement.setAttribute('type', 'password');
    }
  }
}
