/**
 * # Form Helper
 *
 * This module contains methods that directly interact with [Reactive Forms](https://angular.io/guide/reactive-forms)
 *
 * @packageDocumentation
 */

import { AbstractControl, FormGroup } from '@angular/forms';

/**
 * This function mark all controls like touched
 *
 * @param form of type FormGroup
 */
export function controlsMarkAsTouched(form: FormGroup) {
  for (const key in form.controls) {
    if (form.controls.hasOwnProperty(key)) {
      const control = form.controls[key];
      control.markAsTouched();
    }
  }
}

/**
 *
 * @param form AbstractControl
 * @returns Object with attribute isPasswordMatches boolean type
 */
/**
 * This function is selected to evaluate the confirmation of the assignment of a password
 * (password is used in registration flows, password reset or modification of profile data),
 * where the required name of these fields is `password` and` confirmPassword`
 *
 * @param form AbstractControl
 * @return Return a object null or an object with errors
 */
export function passwordMatch(
  form: AbstractControl
): { isPasswordMatches: boolean } | null {
  if (form.get('password').value !== form.get('confirmPassword').value) {
    return { isPasswordMatches: false };
  }
  return null;
}
