import * as GeneralHelper from './general.helper';

describe('General Helper', () => {
  describe('#objectHasPath', () => {
    it('should return false when path is an empty string', () => {
      expect(GeneralHelper.objectHasPath({}, '')).toBe(false);
    });

    it("should return false when path doesn't exist", () => {
      expect(GeneralHelper.objectHasPath({}, 'shallow')).toBe(false);
    });

    it('should return true when shallow path exists', () => {
      expect(GeneralHelper.objectHasPath({ shallow: {} }, 'shallow')).toBe(
        true
      );
    });

    it('should return true when deep path exists', () => {
      expect(
        GeneralHelper.objectHasPath(
          { shallow: { deep: { one: 1 } } },
          'shallow.deep.one'
        )
      ).toBe(true);
    });

    it("should return false when deep path doesn't exist", () => {
      expect(
        GeneralHelper.objectHasPath(
          { shallow: { deep: { one: 1 } } },
          'shallow.deep.two'
        )
      ).toBe(false);
    });
  });

  describe('#hasPropertyPaths', () => {
    it('should return true when called with an empty list of paths', () => {
      expect(GeneralHelper.objectHasPaths({}, [])).toBe(true);
    });

    it('should return true when called with empty path list', () => {
      expect(GeneralHelper.objectHasPaths({}, [])).toBe(true);
    });
  });
});
