/**
 * # File Helper
 *
 * This module contains methods that directly interact with elements of type `File`
 *
 * @packageDocumentation
 */
import { FileValidationErrorType } from '../../enums/file-validation-error-type.enum';
import { FileValidationConfig } from '../../models/file-validation-config.model';

/**
 * This function evaluate a file according of the given configuration
 *
 * @param file File to evaluate
 * @param config Object that contains configurations for evaluate file
 * @returns Return of a response according to evaluated
 */
export function isValidFile(
  file: File,
  config: FileValidationConfig
): FileValidationErrorType {
  if (file) {
    if (file.size <= config.fileMaxSize) {
      if (config.fileTypesAccepted.indexOf(file.type) > -1) {
        return FileValidationErrorType.accepted;
      } else {
        return FileValidationErrorType.typeInvalid;
      }
    } else {
      return FileValidationErrorType.limitExceeded;
    }
  }
  return FileValidationErrorType.errorToRead;
}

/**
 * This function convert a file to `base64`
 *
 * @param file File to convert
 * @returns Promise with reader.result
 */
export function fileToBase64(file: File): Promise<string | ArrayBuffer | null> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onloadend = () => resolve(reader.result);
    reader.onerror = reject;
    reader.readAsDataURL(file);
  });
}
