/**
 * Takes a RegExp and returns only the content, without "/" delimiters or flags
 *
 * ```ts
 * // returns "[a-zA-Z]"
 * stripRegex(/[a-zA-Z]/)
 *
 * // returns "[a-zA-Z]"
 * stripRegex(/[a-zA-Z]/igm)
 * ```
 *
 * @param regex Regular Expresion to extract the content of
 */
const stripRegex = (regex: RegExp): string => {
  const strRegex = regex.toString();
  return strRegex.slice(strRegex.indexOf('/') + 1, strRegex.lastIndexOf('/'));
};

/**
 * Matches a general structure for emails: `'...'@'...'.'...'`
 *
 * This won't check for the validity of domain extensions, or forbidden
 * patterns and characters other than not having more than two `@` in the email.
 *
 * ```ts
 * // returns ["hey@hey.com", index: 0, input: "hey@hey.com", groups: undefined]
 * "hey@hey.com".match(RegexEmail)
 *
 * // returns null
 * "hey@hey@hey.com".match(RegexEmail)
 *
 * // returns null
 * "hey.com".match(RegexEmail)
 * ```
 */
export const RegexEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

/**
 * Matches strings with 1 capital letter, 1 number, 1 symbol, and custom length.
 *
 * When called like `RegexPassword()` it'll use 8 as default minimum length.
 *
 * ```ts
 * // returns ["Abc123+-", index: 0, input: "Abc123+-", groups: undefined]
 * "Abc123+-".match(RegexPassword())
 *
 * // returns null
 * "Abc123+-".match(RegexPassword(100))
 *
 * // returns null
 * "verysecure".match(RegexPassword)
 * ```
 *
 * @param minLength Minimum number of characters for the password
 * @param maxLength Maximum number of characters for the password
 */
export const RegexPassword = (minLength: number = 8, maxLength?: number) =>
  new RegExp(
    `/^((?=.*\d)(?=.*[A-Z])(?=.*\W).{${minLength},${
      maxLength ? maxLength : ''
    }})$/`
  );

/**
 * Matches valid YouTube IDs (11-char base64 with '-' instead of '+' and '_' instead of '/')
 *
 * ```ts
 * // returns ["jNQXAC9IVRw", index: 0, input: "jNQXAC9IVRw", groups: undefined]
 * "jNQXAC9IVRw".match(RegexValidYoutubeId)
 *
 * // returns ["___________", index: 0, input: "___________", groups: undefined]
 * "___________".match(RegexValidYoutubeId)
 *
 * // returns null, length is not 11
 * "12".match(RegexValidYoutubeId)
 *
 * // returns null, length is not 11
 * "01234567890123".match(RegexValidYoutubeId)
 *
 * // returns null, invalid char
 * "jNQXAC9IVR+".match(RegexValidYoutubeId)
 * ```
 */
export const RegexValidYoutubeId = /[A-Za-z0-9_\-]{11}$/;

/**
 * Will obtian the video ID from common YouTube URL structures
 *
 * The returned value `v` will contain:
 *   - at `v[0]` the full url that matched
 *   - at `v[1]` the video ID that matches @{@link RegexValidYoutubeId}
 *
 * Common YouTube URL structures include:
 *
 *  - `'...'youtu.be/{VideoID}`
 *  - `'...'watch/v=?{VideoID}`
 *  - `'...'v/{VideoID}`
 *  - `'...'u/''/{VideoID}`
 *
 * ```ts
 * // returns [
 * //   "https://www.youtube.com/watch?v=jNQXAC9IVRw",
 * //   "jNQXAC9IVRw",
 * //   index: 0, input: "jNQXAC9IVRw", groups: undefined]
 * "https://www.youtube.com/watch?v=jNQXAC9IVRw".match(RegexValidYoutubeId)
 *
 * // returns [
 * //   "https://youtu.be/jNQXAC9IVRw",
 * //   "jNQXAC9IVRw",
 * //   index: 0, input: "jNQXAC9IVRw", groups: undefined]
 * "https://youtu.be/jNQXAC9IVRw".match(RegexValidYoutubeId)
 *
 * // returns null, ID is not valid
 * "https://youtu.be/++-&/(".match(RegexValidYoutubeId)
 *
 * // returns null, structure is not valid
 * "https://yt/jNQXAC9IVRw".match(RegexValidYoutubeId)
 * ```
 *
 * @see {@link RegexValidYoutubeId}
 */
export const RegexYouTubeURL = new RegExp(
  `.*(?:youtu.be\\/|v\\/|u\\/\\w\\/|embed\\/|watch\\?v=)(${stripRegex(
    RegexValidYoutubeId
  )}).*`
);

// Matches valid URLs
export const RegexURL = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/gi;
