/**
 * # Type of error to validate file
 *
 * This `enum` contains distinct types of error to evaluated a file
 */

export enum FileValidationErrorType {
  /**
   *  Indicates that the file is valid and meets the requirements given in the declared configuration
   */
  accepted = 'accepted',
  /**
   *  Indicates that the file size has been exceeded according to the given configuration
   */
  limitExceeded = 'limitExceeded',
  /**
   *  Indicates that the file type in not valid according to the given configuration
   */
  typeInvalid = 'typeInvalid',
  /**
   *  Indicates that the file could not be read
   */
  errorToRead = 'errorToRead',
}
