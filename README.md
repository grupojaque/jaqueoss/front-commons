## About

This package contains some of the utilities for front-end development that we:

- have implemented repeatedly, or
- believe could be useful for someone else.

Its current goal is to be heavily used in all the front-end projects at Jaque.
Minimizing the duplication between any of them.

The contents vary between data structures utilities, map helpers, and sorters,
to fully customizable components, both in style and functionality.

## Installing

Public usage is discouraged since we haven't released a stable version yet, and
anything is susceptible to change.

However, you can install and use this library by running:

```
npm i grupojaque-ngx-commons --save
```

```
yarn add grupojaque-ngx-commons
```

## Usage

Importing the full library must be avoided. You should only include the exports
you plan to use:

```ts
import { RegexPassword } from 'grupojaque-ngx-commons';
```
