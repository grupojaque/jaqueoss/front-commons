module.exports = {
  "docs": {
    "Modules": [
      "modules/_lib_enums_type_of_error_to_validate_file_enum_",
      "modules/_lib_helpers_dom_dom_helper_",
      "modules/_lib_helpers_file_file_helper_",
      "modules/_lib_helpers_form_form_helper_",
      "modules/_lib_helpers_general_helper_",
      "modules/_lib_helpers_regex_regex_helper_",
      "modules/_lib_models_config_to_validation_file_model_",
      "modules/_public_api_",
      "modules/_test_"
    ],
    "Enums": [
      "enums/_lib_enums_type_of_error_to_validate_file_enum_.typeoferrortovalidatefile",
      "enums/_lib_enums_type_of_error_to_validate_file_enum_.typeoferrortovalidatefile"
    ],
    "Classes": [
      "classes/_lib_models_config_to_validation_file_model_.configtovalidationfile",
      "classes/_lib_models_config_to_validation_file_model_.configtovalidationfile"
    ]
  }
};