module.exports = {
  title: "Jaque's front-end common",
  tagline: "Jaque's most common commons",
  url: 'https://grupojaque.gitlab.io',
  baseUrl: '/jaqueoss/front-commons/',
  favicon: 'img/favicon.png',
  organizationName: 'grupojaque', // Usually your GitHub org/user name.
  projectName: 'jaqueoss/front-commons', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'JaqueOSS',
      logo: {
        alt: 'JaqueOSS',
        src:
          'https://assets.gitlab-static.net/uploads/-/system/group/avatar/3288421/Q-OSS.png?width=64',
      },
      links: [
        {
          to: 'docs/index',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
        {
          href: 'https://gitlab.com/grupojaque/jaqueoss',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Style Guide',
              to: 'docs/index',
            },
            {
              label: 'Second Doc',
              to: 'docs/doc2',
            },
          ],
        },
        {
          title: 'Social',
          items: [
            {
              label: 'Home',
              to: 'https://jaque.me/',
            },
            {
              label: 'GitLab',
              href: 'https://gitlab.com/grupojaque/jaqueoss/front-commons',
            },
            {
              label: 'Twitter',
              href: 'https://twitter.com/JaqueMx',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Grupo Jaque. Built with Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl:
            'https://gitlab.com/grupojaque/jaqueoss/front-commons/edit/master/website/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
